# Import libraries and dependencies
import aerospike
import config
from pymongo import MongoClient

# Initiate global variables
as_client = None
mongo_client = None
mongo_db = None

def init():
	global as_client
	global mongo_client
	global mongo_db

	# INIT AEROSPIKE
	as_client = aerospike.client(config.as_config)
	as_client.connect()
	as_client.udf_put(config.LUA_FILE)

	# INIT MONGODB
	mongo_client = MongoClient(config.MONGO_HOST, config.MONGO_PORT)
	mongo_db = mongo_client[config.MONGO_DB]
