local function merge_fn(val1, val2)
	return val1 + val2
end

local function reduce_fn(a, b)
	return map.merge(a, b, merge_fn)
end


local function one(rec)
    return 1
end

local function add(a, b)
    return a + b
end

function count(stream)
    return stream : map(one) : reduce(add);
end




-- TAG STATS
local function map_tag(rec)
	local out = map()
	out['tag_id'] = rec.tag_id
	return out
end

local function aggregate_tag(aggregate, nextitem)
	local tag_id = nextitem['tag_id']
	if tag_id ~= "" then
		aggregate[nextitem['tag_id']] = (aggregate[nextitem['tag_id']] or 0) + 1
	end
	return aggregate
end

function aggregate_tag_stats(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end
	return stream : filter(filter_stats) : map(map_tag) : aggregate(map{}, aggregate_tag) : reduce(reduce_fn);
end


-- CREATIVE STATS
local function map_creative(rec)
	local out = map()
	out['creative_id'] = rec.creative_id
	return out
end

local function aggregate_creative(aggregate, nextitem)
	aggregate[nextitem['creative_id']] = (aggregate[nextitem['creative_id']] or 0) + 1
	return aggregate
end

function aggregate_creative_stats(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_creative) : aggregate(map{}, aggregate_creative) : reduce(reduce_fn);
end



-- Each Tag_Id, Domain Stats (video_imp)

local function map_tag_domain(rec)
	local out= map()
	out['tag_id'] = rec.tag_id
	out['domain'] = rec.domain_url
	--out['tag_id_domain'] = (rec.tag_id or "unknown") .. "::" .. (rec.domain_url or "unknown")
	return out
end

local function aggregate_tag_domain(aggregate, nextitem)
	local tag_id = nextitem['tag_id']
	local domain = nextitem['domain']
	--aggregate[domain] = (aggregate[domain] or 0) + 1
	aggregate[tag_id .. "::" .. domain] = (aggregate[tag_id .. "::" .. domain] or 0) + 1
	return aggregate

end

function aggregate_tag_domain_stats(stream, type_name)

	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_tag_domain) : aggregate(map{}, aggregate_tag_domain) : reduce(reduce_fn);
end



-- Each Tag_Id, Country Stats

local function map_tag_country(rec)
	local out= map()
	out['tag_id'] = rec.tag_id
        out['country_code'] = rec.country_code
	--out['tag_id_country'] = (rec.tag_id or "unknown") .. "::" .. (rec.country_code or "unknown")
	return out
end

local function aggregate_tag_country(aggregate, nextitem)
	local tag_id = nextitem['tag_id']
        local country_code = nextitem['country_code']
	--aggregate[nextitem['tag_id_country']] = (aggregate[nextitem['tag_id_country']] or 0) + 1
	aggregate[tag_id .. "::" .. country_code] = (aggregate[tag_id .. "::" .. country_code] or 0) + 1
	return aggregate

end

function aggregate_tag_country_stats(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_tag_country) : aggregate(map{}, aggregate_tag_country) : reduce(reduce_fn);
end

-- Each Creative_Id, Domain Stats

local function map_creative_domain(rec)
	local out= map()
	out['creative_id_domain'] = (rec.creative_id or "unknown") .. "::" .. (rec.domain_url or "unknown")
	return out
end

local function aggregate_creative_domain(aggregate, nextitem)
	aggregate[nextitem['creative_id_domain']] = (aggregate[nextitem['creative_id_domain']] or 0) + 1
	return aggregate

end

function aggregate_creative_domain_stats(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_creative_domain) : aggregate(map{}, aggregate_creative_domain) : reduce(reduce_fn);
end


-- Each Creative_Id, Country Stats

local function map_creative_country(rec)
	local out= map()
	out['creative_id_country'] = (rec.creative_id or "unknown") .. "::" .. (rec.country_code or "unknown")
	return out
end

local function aggregate_creative_country(aggregate, nextitem)
	aggregate[nextitem['creative_id_country']] = (aggregate[nextitem['creative_id_country']] or 0) + 1
	return aggregate

end

function aggregate_creative_country_stats(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_creative_country) : aggregate(map{}, aggregate_creative_country) : reduce(reduce_fn);
end



--Tag Budget Spent Stats
local function map_tag_spent(rec)
	local out= map()
	out['tag_id'] = rec.tag_id
	out['winning_price'] = rec.winning_price
	out['rev_share'] = rec.rev_share
	out['tag_rate'] = rec.tag_rate
	return out
end


local function aggregate_tag_budget(aggregate, nextitem)
	local tag_id = nextitem['tag_id']
	local winning_price = nextitem['winning_price']
	local rev_share = nextitem['rev_share']
	local tag_rate = nextitem['tag_rate']
	if rev_share == '-1' then
		local budget_spent = (tag_rate / 1000 or 0)
		aggregate[tag_id] = (aggregate[tag_id] or 0) + (budget_spent or 0)
	else
		local budget_spent = ((rev_share * winning_price) / 1000 or 0)
		aggregate[tag_id] = (aggregate[tag_id] or 0) + (budget_spent or 0)	
	end
	return aggregate
end


function aggregate_tag_spent(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_tag_spent) : aggregate(map{}, aggregate_tag_budget) : reduce(reduce_fn);
end


--Creative Budget Spent Stats
local function map_creative_spent(rec)
	local out= map()
	out['creative_id'] = rec.creative_id
	out['winning_price'] = rec.winning_price
	return out
end


local function aggregate_creative_budget(aggregate, nextitem)
	local creative_id = nextitem['creative_id']
	local winning_price = nextitem['winning_price']
	local budget_spent = winning_price / 1000 or 0
	aggregate[creative_id] = (aggregate[creative_id] or 0) + (budget_spent or 0)
	return aggregate
end


function aggregate_creative_spent(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_creative_spent) : aggregate(map{}, aggregate_creative_budget) : reduce(reduce_fn);
end



--Tag-Country Budget Spent Stats
local function map_tag_country_spent(rec)
	local out= map()
	out['tag_id'] = rec.tag_id
	out['country_code'] = rec.country_code
	out['winning_price'] = rec.winning_price
	out['rev_share'] = rec.rev_share
	out['tag_rate'] = rec.tag_rate
	return out
end


local function aggregate_tag_country_budget(aggregate, nextitem)
	local tag_id = nextitem['tag_id']
	local country_code = nextitem['country_code']
	local winning_price = nextitem['winning_price']
	local rev_share = nextitem['rev_share']
	local tag_rate = nextitem['tag_rate']
	if rev_share == '-1' then
		local budget_spent = (tag_rate / 1000 or 0)
		aggregate[tag_id .. "::" .. country_code] = (aggregate[tag_id .. "::" .. country_code] or 0) + (budget_spent or 0)
	else
		local budget_spent = ((rev_share * winning_price) / 1000 or 0)
		aggregate[tag_id .. "::" .. country_code] = (aggregate[tag_id .. "::" .. country_code] or 0) + (budget_spent or 0)
	end
	return aggregate
end


function aggregate_tag_country_spent(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_tag_country_spent) : aggregate(map{}, aggregate_tag_country_budget) : reduce(reduce_fn);
end


--Tag-Domain Budget Spent Stats
local function map_tag_domain_spent(rec)
	local out= map()
	out['tag_id'] = rec.tag_id
	out['domain_url'] = rec.domain_url
	out['winning_price'] = rec.winning_price
	out['rev_share'] = rec.rev_share
	out['tag_rate'] = rec.tag_rate
	return out
end

local function aggregate_tag_domain_budget(aggregate, nextitem)
	local tag_id = nextitem['tag_id']
	local domain_url = nextitem['domain_url']
	local winning_price = nextitem['winning_price']
	local rev_share = nextitem['rev_share']
	local tag_rate = nextitem['tag_rate']
	if rev_share == '-1' then
		local budget_spent = (tag_rate / 1000 or 0)
		aggregate[tag_id .. "::" .. domain_url] = (aggregate[tag_id .. "::" .. domain_url] or 0) + (budget_spent or 0)
	else
		local budget_spent = ((rev_share * winning_price) / 1000 or 0)
		aggregate[tag_id .. "::" .. domain_url] = (aggregate[tag_id .. "::" .. domain_url] or 0) + (budget_spent or 0)
	end
	return aggregate
end

function aggregate_tag_domain_spent(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_tag_domain_spent) : aggregate(map{}, aggregate_tag_domain_budget) : reduce(reduce_fn);
end



--Creative-Domain Budget Spent Stats
local function map_creative_domain_spent(rec)
	local out= map()
	out['creative_id'] = rec.creative_id
	out['domain_url'] = rec.domain_url
	out['winning_price'] = rec.winning_price
	return out
end


local function aggregate_creative_domain_budget(aggregate, nextitem)
	local creative_id = nextitem['creative_id']
	local domain_url = nextitem['domain_url']
	local winning_price = nextitem['winning_price']
	local budget_spent = (winning_price / 1000 or 0)
	aggregate[creative_id .. "::" .. domain_url] = (aggregate[creative_id .. "::" .. domain_url] or 0) + (budget_spent or 0)
	return aggregate
end


function aggregate_creative_domain_spent(stream, type_name)
	local function filter_stats(rec)
		return rec.type == type_name
	end

	return stream : filter(filter_stats) : map(map_creative_domain_spent) : aggregate(map{}, aggregate_creative_domain_budget) : reduce(reduce_fn);
end


--Creative-Country Budget Spent Stats
local function map_creative_country_spent(rec)
        local out= map()
        out['creative_id'] = rec.creative_id
        out['country_code'] = rec.country_code
        out['winning_price'] = rec.winning_price
        return out
end 

local function aggregate_creative_country_budget(aggregate, nextitem)
        local creative_id = nextitem['creative_id']
        local country_code = nextitem['country_code']
        local winning_price = nextitem['winning_price']
	local budget_spent = (winning_price / 1000 or 0)
        aggregate[creative_id .. "::" .. country_code] = (aggregate[creative_id .. "::" .. country_code] or 0) + (budget_spent or 0)
        return aggregate
end


function aggregate_creative_country_spent(stream, type_name)
        local function filter_stats(rec)
                return rec.type == type_name
        end

        return stream : filter(filter_stats) : map(map_creative_country_spent) : aggregate(map{}, aggregate_creative_country_budget) : reduce(reduce_fn);
end 
