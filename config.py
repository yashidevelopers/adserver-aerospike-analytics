# Initiating Aerospike cluster hosts for connection
AEROSPIKE_HOST1 = "169.54.60.144"    #aerospike32
AEROSPIKE_HOST2 = "169.54.60.141"    #aerospike33
AEROSPIKE_HOST3 = "169.54.60.157"    #aerospike34
AEROSPIKE_HOST4 = "169.54.60.147"   #aerospike35
AEROSPIKE_HOST5 = "169.54.60.135"   #aerospike36

AEROSPIKE_PORT = 3000
AEROSPIKE_TIMEOUT = 1000 # milliseconds

# Mongo Server and databases declaration
MONGO_HOST = "158.85.28.126"
MONGO_PORT = 27017
MONGO_DB = "adserver_analytics"

# Lua file location (must pass absolute path)
LUA_FILE = "/opt/adserver-aerospike-analytics/adserver_stats.lua"

as_config = {
	"hosts": [
		(AEROSPIKE_HOST1, AEROSPIKE_PORT),
		(AEROSPIKE_HOST2, AEROSPIKE_PORT),
		(AEROSPIKE_HOST3, AEROSPIKE_PORT),
		(AEROSPIKE_HOST4, AEROSPIKE_PORT),
		(AEROSPIKE_HOST5, AEROSPIKE_PORT)
	],
	"policies": {
		"timeout": AEROSPIKE_TIMEOUT
	}
}


