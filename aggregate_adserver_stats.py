#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Import necessary libraries and dependencies
import time
import conn
import argparse
import sys
from datetime import datetime
from aerospike import predicates as p

# To encode non-ASCII characters
enc = sys.stdin.encoding
NAMESPACE = "adserver_event_log"

# Specify the time length for which the stats are processed in seconds
# eg. ONE_HOUR = 300 seconds, the last 5 mins stats are scheduled to process
# eg. ONE_HOUR = 3600 seconds, the last one hour stats are scheduled to process
# We are now processing every 5 mins, increase/decrease this variable according the processing speed

ONE_HOUR = 300

# GET CURRENT HOUR
current_time = None
current_hour = None
report_time = None

# To map the set names in the aerospike cluster's namespace(adserver_event_log) with 'type' bin in all the sets
type_set_map = {
	"adserver.video.opp": "video_opp",
	"adserver.video.req": "video_req",
	"adserver.video.imp": "video_imp",
	"adserver.video.vt25": "video_vt",
	"adserver.video.vt50": "video_vt",
	"adserver.video.vt75": "video_vt",
	"adserver.video.vt100": "video_vt",
	"adserver.video.click": "video_click",
	"adserver.video.error": "video_error",
}

# To map the set names with the field names in mongo database
type_field_map = {
	"adserver.video.opp": "opps",
	"adserver.video.req": "reqs",
	"adserver.video.imp": "impressions",
	"adserver.video.vt25": "vt_25",
	"adserver.video.vt50": "vt_50",
	"adserver.video.vt75": "vt_75",
	"adserver.video.vt100": "vt_100",
	"adserver.video.click": "clicks",
	"adserver.video.error": "errors",
}

# Initiating global variables for mongo bulkop 
tag_bulkop = None
tag_country_bulkop = None
tag_domain_bulkop = None
creative_bulkop = None
creative_domain_bulkop = None
creative_country_bulkop = None

def print_result(value):
	print value

def process_results(value):
	print(value)
	return

# Processing
def process_creative_stats(results, type_name):
	for key, val in results.items():
		mongo_key = {"report_time": report_time, "creative_id": int(key)}
		doc = {}
		doc["$inc"] = {type_field_map[type_name] : val}
		creative_bulkop.find(mongo_key).upsert().update(doc)

def process_creative_domain_stats(results, type_name):
	for key, val in results.items():
		cid, domain = key.split("::")
		domain = unicode(domain,errors= 'ignore' )
		mongo_key = {"report_time": report_time, "creative_id": int(cid), "domain_url": domain}
		doc = {}
		doc["$inc"] = {type_field_map[type_name] : val}
		creative_domain_bulkop.find(mongo_key).upsert().update(doc)

def process_creative_country_stats(results, type_name):
	for key, val in results.items():
		cid, country_code = key.split("::")
		mongo_key = {"report_time": report_time, "creative_id": int(cid), "country_code": country_code}
		doc = {}
		doc["$inc"] = {type_field_map[type_name] : val}
		creative_country_bulkop.find(mongo_key).upsert().update(doc)

def process_tag_stats(results, type_name):
	for key, val in results.items():
		mongo_key = {"report_time": report_time, "tag_id": key}
		doc = {}
		doc["$inc"] = {type_field_map[type_name] : val}
		tag_bulkop.find(mongo_key).upsert().update(doc)

def process_tag_domain_stats(results, type_name):
	for key, val in results.items():
		tid, domain = key.split("::")
		domain = unicode(domain,errors= 'ignore' )
		mongo_key = {"report_time": report_time, "tag_id": tid, "domain_url": domain}
		doc = {}
		doc["$inc"] = {type_field_map[type_name] : val}
		tag_domain_bulkop.find(mongo_key).upsert().update(doc)

def process_tag_country_stats(results, type_name):
	for key, val in results.items():
		tid, country_code = key.split("::")
		mongo_key = {"report_time": report_time, "tag_id": tid, "country_code": country_code}
		doc = {}
		doc["$inc"] = {type_field_map[type_name] : val}
		tag_country_bulkop.find(mongo_key).upsert().update(doc)

def process_tag_spent(results, type_name):
	for key, val in results.items():
		mongo_key = {"report_time": report_time, "tag_id": key}
		doc = {"$inc": {"budget_spent": val}}
		tag_bulkop.find(mongo_key).upsert().update(doc)

def process_tag_country_spent(results, type_name):
	for key, val in results.items():
		tid, country_code = key.split("::")
		mongo_key = {"report_time": report_time, "tag_id": tid, "country_code": country_code}
		doc = {"$inc": {"budget_spent": val}}
		tag_country_bulkop.find(mongo_key).upsert().update(doc)

def process_tag_domain_spent(results, type_name):
	for key, val in results.items():
		tid, domain = key.split("::")
		domain = unicode(domain,errors= 'ignore' )
		mongo_key = {"report_time": report_time, "tag_id": tid, "domain_url": domain}
		doc = {"$inc": {"budget_spent": val}}
		tag_domain_bulkop.find(mongo_key).upsert().update(doc)

def process_creative_spent(results, type_name):
	for key, val in results.items():
		mongo_key = {"report_time": report_time, "creative_id": int(key)}
		doc = {"$inc": {"budget_spent": val}}
		creative_bulkop.find(mongo_key).upsert().update(doc)

def process_creative_domain_spent(results, type_name):
	for key, val in results.items():
		cid, domain = key.split("::")
		domain = unicode(domain,errors= 'ignore' )
		mongo_key = {"report_time": report_time, "creative_id": int(cid), "domain_url": domain}
		doc = {"$inc": {"budget_spent": val}}
		creative_domain_bulkop.find(mongo_key).upsert().update(doc)

def process_creative_country_spent(results, type_name):
        for key, val in results.items():
                cid, country_code = key.split("::")
                mongo_key = {"report_time": report_time, "creative_id": int(cid), "country_code": country_code}
                doc = {"$inc": {"budget_spent": val}}
                creative_country_bulkop.find(mongo_key).upsert().update(doc)

# Aggregations
def agg_creative_stats(type_name):
	print("Agg creative for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_creative_stats', [type_name,])
	query.foreach(lambda x: process_creative_stats(x, type_name))

def agg_creative_domain_stats(type_name):
	print("Agg creative_domain for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_creative_domain_stats', [type_name,])
	query.foreach(lambda x: process_creative_domain_stats(x, type_name))

def agg_creative_country_stats(type_name):
	print("Agg creative_country for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_creative_country_stats', [type_name,])
	query.foreach(lambda x: process_creative_country_stats(x, type_name))

def agg_tag_stats(type_name):
	print("Agg tag for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_tag_stats', [type_name,])
	query.foreach(lambda x: process_tag_stats(x, type_name))

def agg_tag_domain_stats(type_name):
	print("Agg tag_domain for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_tag_domain_stats', [type_name,])
	query.foreach(lambda x: process_tag_domain_stats(x, type_name))

def agg_tag_country_stats(type_name):
	print("Agg tag_country for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_tag_country_stats', [type_name,])
	query.foreach(lambda x: process_tag_country_stats(x, type_name))
	
def agg_tag_spent(type_name):
	print("Agg Tag Spent for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_tag_spent', [type_name,])
	query.foreach(lambda x: process_tag_spent(x, type_name))

def agg_creative_spent(type_name):
	print("Agg Creative Spent for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_creative_spent', [type_name,])
	query.foreach(lambda x: process_creative_spent(x, type_name))

def agg_tag_country_spent(type_name):
	print("Agg Tag-Country Spent for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_tag_country_spent', [type_name,])
	query.foreach(lambda x: process_tag_country_spent(x, type_name))

def agg_tag_domain_spent(type_name):
	print("Agg Tag-Domain Spent for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_tag_domain_spent', [type_name,])
	query.foreach(lambda x: process_tag_domain_spent(x, type_name))

def agg_creative_domain_spent(type_name):
	print("Agg Creative-Domain Spent for {0}...".format(type_name))
	query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
	query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
	query.apply('adserver_stats', 'aggregate_creative_domain_spent', [type_name,])
	query.foreach(lambda x: process_creative_domain_spent(x, type_name))

def agg_creative_country_spent(type_name):
        print("Agg Creative-Country Spent for {0}...".format(type_name))
        query = conn.as_client.query(NAMESPACE, type_set_map[type_name])
        query.where(p.between("time", current_hour, current_hour+ONE_HOUR))
        query.apply('adserver_stats', 'aggregate_creative_country_spent', [type_name,])
        query.foreach(lambda x: process_creative_country_spent(x, type_name))

agg_arr = [

	####################################################################################################################
	# The processing of opps and reqs is on aerospike33
	# ("adserver.video.opp", [agg_tag_stats, agg_tag_domain_stats, agg_tag_country_stats]),
	# ("adserver.video.req", [agg_tag_stats, agg_creative_stats, agg_tag_domain_stats, agg_tag_country_stats, 
					  agg_creative_domain_stats, agg_creative_country_stats]),
	####################################################################################################################

	("adserver.video.imp", [agg_tag_stats, agg_tag_domain_stats, agg_tag_domain_spent, agg_creative_country_spent,
					   agg_tag_country_stats, agg_creative_stats, agg_creative_domain_stats,
					   agg_creative_country_stats, agg_creative_spent,
					   agg_creative_domain_spent, agg_tag_spent, agg_tag_country_spent]),
	("adserver.video.vt25", [agg_tag_stats, agg_creative_stats]),
	("adserver.video.vt50", [agg_tag_stats, agg_creative_stats]),
	("adserver.video.vt75", [agg_tag_stats, agg_creative_stats]),
	("adserver.video.vt100", [agg_tag_stats, agg_creative_stats, agg_creative_domain_stats,
						 agg_creative_country_stats, agg_tag_domain_stats, agg_tag_country_stats]),
	("adserver.video.click", [agg_tag_stats, agg_creative_stats, agg_creative_domain_stats, agg_creative_country_stats,
						 agg_tag_domain_stats, agg_tag_country_stats]),
	("adserver.video.error", [agg_creative_stats]),
 ]


def agg_stats():
	for type_name, funcs in agg_arr:
		for func in funcs:
			func(type_name)


def run_stats(report_time,current_hour):
		global tag_bulkop
		global tag_country_bulkop
		global tag_domain_bulkop
		global creative_bulkop
		global creative_domain_bulkop
		global creative_country_bulkop

		global agg_arr

		conn.init()

		tag_bulkop = conn.mongo_db.tag_stats.initialize_ordered_bulk_op()
		tag_country_bulkop = conn.mongo_db.tag_country_stats.initialize_ordered_bulk_op()
		tag_domain_bulkop = conn.mongo_db.tag_domain_stats.initialize_ordered_bulk_op()
		creative_bulkop = conn.mongo_db.creative_stats.initialize_ordered_bulk_op()
		creative_domain_bulkop = conn.mongo_db.creative_domain_stats.initialize_ordered_bulk_op()
		creative_country_bulkop = conn.mongo_db.creative_country_stats.initialize_ordered_bulk_op()

		print(report_time)
		print(datetime.utcfromtimestamp(current_hour))
		print(datetime.utcfromtimestamp(current_hour+ONE_HOUR))
		
		agg_stats()
		print("Aggregation done!")
		
				
		try:
			tag_bulkop.execute()
		except Exception as e:
			print("Exception_tag: " + str(e))
		try:
			tag_country_bulkop.execute()
		except Exception as e:
			print("Exception_tag_country: " + str(e))
		try:
			tag_domain_bulkop.execute()
		except Exception as e:
			print("Exception_tag_domain: " + str(e))
		try:
			creative_bulkop.execute()
		except Exception as e:
			print("Exception_creative: " + str(e))
		try:
			creative_domain_bulkop.execute()
		except Exception as e:
			print("Exception_creative_domain: " + str(e))
		try:
			creative_country_bulkop.execute()
		except Exception as e:
			print("Exception_creative_country: " + str(e))
		

def main():
	global report_time
	global current_time
	global current_hour

	# Set up args
	parser = argparse.ArgumentParser(description='Aerospike Analytics')
	parser.add_argument('--lasthour', action='store_true')
	parser.add_argument('--allstats', action='store_true')
	parser.add_argument('--genstats', action='store_true')
	parser.add_argument('--zipstats', action='store_true')
	parser.add_argument('--exchangestats', action='store_true')
	parser.add_argument('--domainstats', action='store_true')
	parser.add_argument('--segmentstats', action='store_true')
	parser.add_argument('--starttime', type=int)
	parser.add_argument('--hours', type = int)
	args = parser.parse_args()

	current_time = int(time.time())

	if args.lasthour:
		current_hour = current_time - ONE_HOUR - (current_time % ONE_HOUR)
		report_Time = datetime.utcfromtimestamp(current_hour)
		report_time = datetime(report_Time.year,report_Time.month, report_Time.day, report_Time.hour)
		run_stats(report_time,current_hour)
	elif args.starttime:
		current_hour = (args.starttime)
		report_Time = datetime.utcfromtimestamp(current_hour)
		report_time = datetime(report_Time.year,report_Time.month, report_Time.day, report_Time.hour)
		run_stats(report_time,current_hour)
	elif args.hours:
		for hour in range(1,args.hours+1):
			current_hour = current_time - (current_time % (ONE_HOUR)) - (hour * ONE_HOUR)
			print(hour)
			print(current_time)
			print(current_hour)
			report_time = datetime.utcfromtimestamp(current_hour)
			run_stats(report_time,current_hour)
	else:
		current_hour = current_time - (current_time % ONE_HOUR)
		report_time = datetime.utcfromtimestamp(current_hour)
		run_stats(report_time,current_hour)



if __name__ == '__main__':
	main()
